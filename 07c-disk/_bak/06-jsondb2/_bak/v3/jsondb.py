import json
import re
import os

INT_SIZE = 4
BLOCK_SIZE = 4096

def readInt(file):
    ibytes = file.read(INT_SIZE)
    return int.from_bytes(ibytes)

def writeInt(file, n):
    file.write(n.to_bytes(INT_SIZE))

class QueryResults:
    def __init__(self, items):
        self.items = items

    def filter(self, f):
        fitems = filter(f, self.items)
        return QueryResults(fitems)

    def where(self, f):
        return self.filter(lambda x:f(x['obj']))
    
    def match(self, q):
        return self.filter(lambda x:x['doc'].find(q)>=0)

    def toList(self):
        return list(map(lambda x:x['obj'], self.items))

    def __str__(self):
        return str(self.toList())

class Index:
    def __init__(self):
        self.size = 0
        self.idx = []

    def read(self, idxFile):
        self.size = readInt(idxFile)
        for i in range(self.size): # range(BLOCK_SIZE//INT_SIZE):
            offset = readInt(idxFile)
            if offset != 0:
                self.add(offset)

    def write(self, idxFile):
        # 當超過一個區塊時，必須處理區塊串列的事情，目前沒有做
        # 因此最多只輸出一個區塊 (所以沒辦法全部資料都索引找到)
        n = min(len(self.idx), BLOCK_SIZE//INT_SIZE-2) # 頭尾不能存 idx，是留給 size 和 next 的
        writeInt(idxFile, self.size)
        for i in range(n):
            writeInt(idxFile, self.idx[i])

    def add(self, offset):
        if not offset in self.idx:
            self.idx.append(offset)
            self.size += 1

class JsonDB:
    def __init__(self):
        pass

    def open(self, path='./jdb/', hashSize=10000):
        self.invIdx = {}
        self.path = path
        idxFileName = f'{self.path}/jdb.idx'
        dataFileName = f'{self.path}/jdb.data'

        if os.path.isdir(path):
            self.idxFile = open(idxFileName, 'r+b')
            self.dataFile = open(dataFileName, 'r+', encoding='utf8')
            doc= self.readDoc(0)
            self.meta = json.loads(doc)
            self.dataSize = os.path.getsize(dataFileName)
        else:
            os.mkdir(path)
            self.idxFile = open(idxFileName, "w+b")
            self.idxFile.seek(BLOCK_SIZE*hashSize-1)
            self.idxFile.write(b"\0")
            self.idxFile.close()
            assert(os.path.getsize(idxFileName)==BLOCK_SIZE*hashSize)
            self.idxFile = open(idxFileName, "r+b")
            self.dataFile = open(dataFileName, "w+", encoding='utf8')
            self.dataSize = 0
            self.meta = {'db':'jsondb', 'hashSize':hashSize, 'blockSize': BLOCK_SIZE, 'intSize': INT_SIZE}
            self.addObj(self.meta)

    def close(self):
        self.flush()
        self.idxFile.close()
        self.dataFile.close()

    def flush(self):
        for w, idx in self.invIdx.items():
            self.writeIdx(w, idx)

    def hash(self, s): # python 的 hash 每個 session 都不一樣 (為了防駭客攻擊)，所以不能用
        s = s.lower()
        h = 0
        if len(s) == 0: return h
        for i in range(len(s)):
            c = s[i]
            h = ((h << 5) - h) + ord(c) # hash = hash*31 + chr = (hash*32-hash) + c
            h = int(h)
        return h%self.meta["hashSize"]

    def readIdx(self, whash):
        offset = whash*BLOCK_SIZE
        self.idxFile.seek(offset)
        index = Index()
        index.read(self.idxFile)
        return index

    def writeIdx(self, whash, index):
        offset = whash*BLOCK_SIZE
        self.idxFile.seek(whash*BLOCK_SIZE)
        index.write(self.idxFile)

    def readDoc(self, offset=0):
        self.dataFile.seek(offset)
        doc = self.dataFile.readline()
        return doc.replace('\n', '')

    def writeDoc(self, doc):
        self.dataFile.seek(self.dataSize)
        record = doc.replace("\n", "\\n")+'\n'
        self.dataFile.write(record)
        pos = self.dataSize
        self.dataSize += len(str.encode(record))
        return pos

    def addObj(self, obj):
        doc = json.dumps(obj, ensure_ascii=False)
        pos = self.writeDoc(doc)
        self.indexDoc(doc, pos)

    def addIdx(self, word, pos):
        whash = self.hash(word)
        index = self.invIdx.get(whash)
        if not index:
            index = Index()
        index.add(pos)
        self.invIdx[whash] = index

    def indexDoc(self, doc, pos):
        dlen = len(doc)
        i = 0
        while i<dlen:
            eword = re.compile("([a-zA-Z]+)|([0-9]+)")
            m = eword.match(doc, i)
            word = None
            if m:
                word = m.group(0).lower()
                self.addIdx(word, pos)
                i+=len(word)
            else:
                cword = re.compile("[\u4e00-\u9fff]{2,3}") # https://stackoverflow.com/questions/2718196/find-all-chinese-text-in-a-string-using-python-and-regex
                m = cword.match(doc, i)
                if m:
                    word = m.group(0)
                    for wlen in range(1, len(word)+1):
                        self.addIdx(word[0:wlen], pos)
                i+=1

    def match(self, q):
        q = q.lower()
        regexp = r"([\u4e00-\u9fff]{2,3}|[a-zA-Z]{2,}|[0-9]{2,})"
        words = re.findall(regexp, q)
        words.sort(key = lambda x:len(x), reverse=True)
        if len(words)==0: return None
        whash = self.hash(words[0])
        index = self.readIdx(whash)
        r = []
        for offset in index.idx:
            doc = self.readDoc(offset)
            if doc.lower().find(q)>=0:
                r.append({'obj':json.loads(doc), 'doc':doc})
        return QueryResults(r)
