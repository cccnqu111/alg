import json
import re
import os

INT_SIZE = 4

class QueryResults:
    def __init__(self, items):
        self.items = items

    def filter(self, f):
        fitems = filter(f, self.items)
        return QueryResults(fitems)

    def where(self, f):
        return self.filter(lambda x:f(x['obj']))
    
    def match(self, q):
        return self.filter(lambda x:x['doc'].find(q)>=0)

    def toList(self):
        return list(map(lambda x:x['obj'], self.items))

    def __str__(self):
        return str(self.toList())

class JsonDB:
    def __init__(self):
        pass

    def open(self, path='./jdb/', hashSize=10000, blockSize=4096):
        self.invIdx = {}
        self.path = path
        metaFileName = f'{self.path}/jdb.meta'
        idxFileName = f'{self.path}/jdb.idx'
        dataFileName = f'{self.path}/jdb.data'

        if os.path.isdir(path):
            self.idxFile = open(idxFileName, 'r+b')
            self.dataFile = open(dataFileName, 'r+', encoding='utf8')
            doc= self.readDoc(0)
            self.meta = json.loads(doc)
            self.dataSize = os.path.getsize(dataFileName)
        else:
            os.mkdir(path)
            self.idxFile = open(idxFileName, "w+b")
            self.idxFile.seek(blockSize*hashSize-1)
            self.idxFile.write(b"\0")
            self.idxFile.close()
            assert(os.path.getsize(idxFileName)==blockSize*hashSize)
            self.idxFile = open(idxFileName, "r+b")
            self.dataFile = open(dataFileName, "w+", encoding='utf8')
            self.dataSize = 0
            self.meta = {'db':'jsondb', 'hashSize':hashSize, 'blockSize': blockSize}
            self.add(self.meta)

    def close(self):
        self.flush()
        self.idxFile.close()
        self.dataFile.close()

    def flush(self):
        for w, idx in self.invIdx.items():
            self.writeIdx(w, idx)

    def hash(self, s): # python 的 hash 每個 session 都不一樣 (為了防駭客攻擊)，所以不能用
        s = s.lower()
        h = 0
        if len(s) == 0: return h
        for i in range(len(s)):
            c = s[i]
            h = ((h << 5) - h) + ord(c) # hash = hash*31 + chr = (hash*32-hash) + c
            h = int(h)
        return h%self.meta["hashSize"]

    def readIdx(self, whash):
        blockSize = self.meta['blockSize']
        offset = whash*blockSize
        self.idxFile.seek(offset)
        idx = []
        for i in range(blockSize//INT_SIZE):
            idxBytes = self.idxFile.read(INT_SIZE)
            num = int.from_bytes(idxBytes)
            if num != 0:
                idx.append(num)
        return idx

    def writeIdx(self, whash, idx):
        blockSize = self.meta['blockSize']
        offset = whash*blockSize
        self.idxFile.seek(whash*blockSize)
        maxIdx = blockSize//INT_SIZE
        assert(len(idx)<=maxIdx)
        for num in idx:
            self.idxFile.write(num.to_bytes(INT_SIZE))

    def readDoc(self, offset=0):
        self.dataFile.seek(offset)
        doc = self.dataFile.readline()
        return doc.replace('\n', '')

    def writeDoc(self, doc):
        self.dataFile.seek(self.dataSize)
        record = doc.replace("\n", "\\n")+'\n'
        self.dataFile.write(record)
        pos = self.dataSize
        self.dataSize += len(str.encode(record))
        return pos

    def add(self, obj):
        doc = json.dumps(obj, ensure_ascii=False)
        pos = self.writeDoc(doc)
        self.index(doc, pos)

    # def appendIdxBlock(idx):

    def addIdx(self, word, pos):
        whash = self.hash(word)
        idx = self.invIdx.get(whash)
        if idx:
            # 當超過一個區塊時，必須處理區塊串列的事情，目前沒有做
            # blockSize = self.meta['blockSize']
            # if len(idx) == blockSize//INT_SIZE-1:
                # appendIdxBlock(idx)
                # self.writeIdx(whash, idx)
            blockSize = self.meta['blockSize']
            maxIdx = blockSize//INT_SIZE
            # if len(idx)>=maxIdx:
            #    raise Exception(f'block full:word={word}')
            if len(idx)<maxIdx:
                if not pos in idx:
                    idx.append(pos)
        else:
            self.invIdx[whash] = [pos]

    def index(self, doc, pos):
        dlen = len(doc)
        i = 0
        while i<dlen:
            eword = re.compile("([a-zA-Z]+)|([0-9]+)")
            m = eword.match(doc, i)
            word = None
            if m:
                word = m.group(0).lower()
                self.addIdx(word, pos)
                i+=len(word)
            else:
                cword = re.compile("[\u4e00-\u9fff]{2,3}") # https://stackoverflow.com/questions/2718196/find-all-chinese-text-in-a-string-using-python-and-regex
                m = cword.match(doc, i)
                if m:
                    word = m.group(0)
                    for wlen in range(1, len(word)+1):
                        self.addIdx(word[0:wlen], pos)
                i+=1

    def match(self, q):
        q = q.lower()
        regexp = r"([\u4e00-\u9fff]{2,3}|[a-zA-Z]{2,}|[0-9]{2,})"
        words = re.findall(regexp, q)
        words.sort(key = lambda x:len(x), reverse=True)
        if len(words)==0: return None
        whash = self.hash(words[0])
        idx = self.readIdx(whash)
        r = []
        for offset in idx:
            doc = self.readDoc(offset)
            if doc.lower().find(q)>=0:
                r.append({'obj':json.loads(doc), 'doc':doc})
        return QueryResults(r)
