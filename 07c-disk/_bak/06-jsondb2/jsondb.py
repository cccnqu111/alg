import json
import re
import os

INT_SIZE = 4
BLOCK_SIZE = 4096
assert(BLOCK_SIZE%INT_SIZE==0)

HASH_SIZE = 16384
HEAD_RECORD_SIZE = INT_SIZE # (head record={first_block_offset})
HEAD_SIZE = HASH_SIZE*HEAD_RECORD_SIZE
IDX_IN_BLOCK = BLOCK_SIZE//INT_SIZE-1 # 
assert(HASH_SIZE*INT_SIZE%BLOCK_SIZE==0)

def readInt(file):
    ibytes = file.read(INT_SIZE)
    return int.from_bytes(ibytes)

def writeInt(file, n):
    file.write(n.to_bytes(INT_SIZE))

def fileSize(file):
    file.seek(0, os.SEEK_END)
    return file.tell()

class QueryResults:
    def __init__(self, items):
        self.items = items

    def filter(self, f):
        fitems = filter(f, self.items)
        return QueryResults(fitems)

    def where(self, f):
        return self.filter(lambda x:f(x['obj']))
    
    def match(self, q):
        return self.filter(lambda x:x['doc'].find(q)>=0)

    def toList(self):
        return list(map(lambda x:x['obj'], self.items))

    def __str__(self):
        return str(self.toList())

class Index:
    def __init__(self):
        self.firstOffset = 0
        self.readed = False
        self.idx = []

    def blocks(self):
        nblock = (len(self.idx)+IDX_IN_BLOCK-1)//IDX_IN_BLOCK
        return nblock

    def read(self, indexFile):
        if self.readed: return # 已經讀過了，直接傳回
        indexFile.seek(self.firstOffset)
        for b in range(self.blocks()):
            for _ in range(IDX_IN_BLOCK):
                offset = readInt(indexFile)
                if offset != 0:
                    self.idx.append(offset)
            nextOffset = readInt(indexFile)
            indexFile.seek(nextOffset)

    def write(self, indexFile):
        idxLen = len(self.idx)
        if idxLen == 0: return # 沒有資料，不用寫入。
        i = 0
        nblock = self.blocks()
        for b in range(nblock):
            indexFile.seek(0, os.SEEK_END)
            nextOffset = indexFile.tell()
            for _ in range(IDX_IN_BLOCK):
                if i < idxLen:
                    writeInt(indexFile, self.idx[i])
                else:
                    writeInt(indexFile, 0)
                i += 1
            if b == 0:
                self.firstOffset = nextOffset
            elif b == nblock-1:
                writeInt(indexFile, 0) # 最後一個區塊，沒有 next block
            else:
                writeInt(indexFile, indexFile.tell()+INT_SIZE) # 下一個區塊從此開始。

    def add(self, offset):
        if not offset in self.idx:
            self.idx.append(offset)

class JsonDB:
    def __init__(self):
        pass

    def open(self, path='./jdb/'):
        self.index = [Index() for _ in range(HASH_SIZE)]
        self.path = path
        indexFileName = f'{self.path}/jdb.idx'
        dataFileName = f'{self.path}/jdb.data'

        if os.path.isdir(path):
            self.indexFile = open(indexFileName, 'r+b')
            self.dataFile = open(dataFileName, 'r+', encoding='utf8')
            doc= self.readDoc(0)
            self.meta = json.loads(doc)
            self.dataSize = os.path.getsize(dataFileName)
            self.readHead()
        else:
            os.mkdir(path)
            self.indexFile = open(indexFileName, "w+b")
            self.indexFile.seek(HEAD_SIZE-1)
            self.indexFile.write(b"\0")
            self.indexFile.close()
            assert(os.path.getsize(indexFileName)==HEAD_SIZE)
            self.indexFile = open(indexFileName, "r+b")
            self.dataFile = open(dataFileName, "w+", encoding='utf8')
            self.dataSize = 0
            self.meta = {'db':'jsondb', 'hashSize':HASH_SIZE, 'blockSize': BLOCK_SIZE, 'intSize': INT_SIZE}
            self.addObj(self.meta)

    def close(self):
        self.indexFile.close()
        self.dataFile.close()

    def readHead(self):
        self.indexFile.seek(0)
        for w in range(HASH_SIZE):
            self.index[w].firstOffset = readInt(self.indexFile)

    def writeHead(self):
        self.indexFile.seek(0)
        for w in range(HASH_SIZE):
            writeInt(self.indexFile, self.index[w].firstOffset)
            if self.index[w].firstOffset != 0:
                print(f'head:{w} firstOffset={self.index[w].firstOffset}')

    def save(self):
        for w in range(HASH_SIZE):
            self.writeIndex(w, self.index[w])
        self.writeHead()

    def hash(self, s): # python 的 hash 每個 session 都不一樣 (為了防駭客攻擊)，所以不能用
        s = s.lower()
        h = 0
        if len(s) == 0: return h
        for i in range(len(s)):
            c = s[i]
            h = ((h << 5) - h) + ord(c) # hash = hash*31 + chr = (hash*32-hash) + c
            h = int(h)
        return h%HASH_SIZE

    def readIndex(self, whash):
        index = self.index[whash]
        print(f'readIndex:whash={whash} firstOffset={index.firstOffset}')
        index.read(self.indexFile)
        return index

    def writeIndex(self, whash, index):
        index.write(self.indexFile)

    def readDoc(self, offset=0):
        self.dataFile.seek(offset)
        doc = self.dataFile.readline()
        return doc.replace('\n', '')

    def writeDoc(self, doc):
        self.dataFile.seek(self.dataSize)
        record = doc.replace("\n", "\\n")+'\n'
        self.dataFile.write(record)
        pos = self.dataSize
        self.dataSize += len(str.encode(record))
        return pos

    def addObj(self, obj):
        doc = json.dumps(obj, ensure_ascii=False)
        pos = self.writeDoc(doc)
        self.indexDoc(doc, pos)

    def indexWord(self, word, pos):
        whash = self.hash(word)
        index = self.index[whash]
        index.add(pos)

    def indexDoc(self, doc, pos):
        dlen = len(doc)
        i = 0
        while i<dlen:
            eword = re.compile("([a-zA-Z]+)|([0-9]+)")
            m = eword.match(doc, i)
            word = None
            if m:
                word = m.group(0).lower()
                self.indexWord(word, pos)
                i+=len(word)
            else:
                cword = re.compile("[\u4e00-\u9fff]{2,3}") # https://stackoverflow.com/questions/2718196/find-all-chinese-text-in-a-string-using-python-and-regex
                m = cword.match(doc, i)
                if m:
                    word = m.group(0)
                    for wlen in range(1, len(word)+1):
                        self.indexWord(word[0:wlen], pos)
                i+=1

    def match(self, q):
        q = q.lower()
        regexp = r"([\u4e00-\u9fff]{2,3}|[a-zA-Z]{2,}|[0-9]{2,})"
        words = re.findall(regexp, q)
        words.sort(key = lambda x:len(x), reverse=True)
        if len(words)==0: return None
        whash = self.hash(words[0])
        index = self.readIndex(whash)
        r = []
        for offset in index.idx:
            doc = self.readDoc(offset)
            if doc.lower().find(q)>=0:
                r.append({'obj':json.loads(doc), 'doc':doc})
        return QueryResults(r)
