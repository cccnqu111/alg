n = 9
words = "a ew vc tea oe eat zoo el le".split()
print('words=', words)

def normalize(word):
    chars = list(word)
    chars.sort()
    return ''.join(chars)

# print(normalize('eat'))
countMap = {}
for word in words:
    nword = normalize(word)
    if countMap.get(nword) is None:
        countMap[nword] = 1
    else:
        countMap[nword] += 1
print('countMap=', countMap)
hits = []
for word, count in countMap.items():
    if (count >= 2):
        hits.append(word)
print('hits=', hits)