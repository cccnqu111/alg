from jsondb import JsonDB

jdb = JsonDB()
jdb.open()
# r = jdb.match('unix')
# print('unix:', r)
r = jdb.match('unix').match('檔案').where(lambda x:x['id']<50).toList()
print('unix&檔案&id<50:', r)
r = jdb.match('愛因斯坦').where(lambda x:x['id']<100).match('納粹').toList()
print('愛因斯坦&id<100&納粹:', r)
